## Discord-slash-components
[discord-py-slash-command](https://github.com/discord-py-slash-commands/discord-py-slash-command) and [discord-components](https://pypi.org/project/discord-components/) are cool, but they don't support each other. So I've made a connector between them. So if you want to use both librarys, use this!

And [discord-py-slash-command](https://github.com/discord-py-slash-commands/discord-py-slash-command) supports components on their components branch so you could use them 👍