from discord import Client, Embed, File, AllowedMentions, Object
from discord.ext.commands import Bot

from discord_components import DiscordComponents as _DiscordComponents, Component
from discord_slash import SlashCommand, SlashContext
from discord_slash.error import IncorrectFormat
from discord_slash.model import SlashMessage

from typing import Union, Optional, List

from .message import ComponentSlashMessage

__all__ = ("DiscordComponents",)


class DiscordComponents(_DiscordComponents):
    def __init__(
        self,
        bot: Union[Client, Bot],
        slash: Optional[SlashCommand] = None,
        *args,
        change_slash_methods: bool = True,
        **kwargs
    ):
        super().__init__(bot, *args, **kwargs)
        self.slash = slash

        if change_slash_methods:
            self.change_slash_methods()

    def change_slash_methods(self):
        async def send_slash_component_msg_prop(ctx, *args, **kwargs):
            return await self.send_slash_component_message(ctx, *args, **kwargs)

        SlashContext.send = send_slash_component_msg_prop
        SlashMessage._slash_edit = self.edit_slash_component_message

    async def send_slash_component_message(
        self,
        context: SlashContext,
        content: str = "",
        *,
        embed: Embed = None,
        embeds: List[Embed] = None,
        tts: bool = False,
        file: File = None,
        files: List[File] = None,
        allowed_mentions: AllowedMentions = None,
        hidden: bool = False,
        delete_after: float = None,
        components: List[Component] = None
    ) -> ComponentSlashMessage:
        if embed and embeds:
            raise IncorrectFormat("You can't use both `embed` and `embeds`!")
        if embed:
            embeds = [embed]
        if embeds:
            if not isinstance(embeds, list):
                raise IncorrectFormat("Provide a list of embeds.")
            elif len(embeds) > 10:
                raise IncorrectFormat("Do not provide more than 10 embeds.")
        if file and files:
            raise IncorrectFormat("You can't use both `file` and `files`!")
        if file:
            files = [file]
        if delete_after and hidden:
            raise IncorrectFormat("You can't delete a hidden message!")

        base = {
            "content": content,
            "tts": tts,
            "embeds": [x.to_dict() for x in embeds] if embeds else [],
            "allowed_mentions": allowed_mentions.to_dict()
            if allowed_mentions
            else self.bot.allowed_mentions.to_dict()
            if self.bot.allowed_mentions
            else {},
            **self._get_components_json(components),
        }
        if hidden:
            base["flags"] = 64

        initial_message = False
        if not context.responded:
            initial_message = True
            if files and not context.deferred:
                await context.defer(hidden=hidden)
            if context.deferred:
                if context._deferred_hidden != hidden:
                    context._logger.warning(
                        "Deferred response might not be what you set it to! (hidden / visible) "
                        "This is because it was deferred in a different state."
                    )
                resp = await context._http.edit(base, context._SlashContext__token, files=files)
                context.deferred = False
            else:
                json_data = {"type": 4, "data": base}
                await context._http.post_initial_response(
                    json_data, context.interaction_id, context._SlashContext__token
                )
                if not hidden:
                    resp = await context._http.edit({}, context._SlashContext__token)
                else:
                    resp = {}
            context.responded = True
        else:
            resp = await context._http.post_followup(
                base, context._SlashContext__token, files=files
            )
        if files:
            for file in files:
                file.close()
        if not hidden:
            smsg = ComponentSlashMessage(
                state=self.bot._connection,
                data=resp,
                channel=context.channel or Object(id=context.channel_id),
                _http=context._http,
                interaction_token=context._SlashContext__token,
            )
            if delete_after:
                self.bot.loop.create_task(smsg.delete(delay=delete_after))
            if initial_message:
                self.message = smsg
            return smsg
        else:
            return resp

    async def edit_slash_component_message(
        self, message: ComponentSlashMessage, **fields
    ) -> SlashMessage:
        state = self.bot._get_state()
        data = {}

        content = fields.get("content")
        if content:
            data["content"] = str(content)

        embed = fields.get("embed")
        embeds = fields.get("embeds")
        file = fields.get("file")
        files = fields.get("files")
        components = fields.get("components")

        if components:
            data["components"] = components

        if embed and embeds:
            raise IncorrectFormat("You can't use both `embed` and `embeds`!")
        if file and files:
            raise IncorrectFormat("You can't use both `file` and `files`!")
        if file:
            files = [file]
        if embed:
            embeds = [embed]
        if embeds:
            if not isinstance(embeds, list):
                raise IncorrectFormat("Provide a list of embeds.")
            elif len(embeds) > 10:
                raise IncorrectFormat("Do not provide more than 10 embeds.")
            data["embeds"] = [x.to_dict() for x in embeds]

        allowed_mentions = fields.get("allowed_mentions")
        data["allowed_mentions"] = (
            allowed_mentions.to_dict()
            if allowed_mentions
            else state.allowed_mentions.to_dict()
            if state.allowed_mentions
            else {}
        )

        await message._http.edit(
            data, message.__SlashMessage_interaction_token, message.id, files=files
        )

        delete_after = fields.get("delete_after")
        if delete_after:
            await message.delete(delay=delete_after)
        if files:
            [x.close() for x in files]
