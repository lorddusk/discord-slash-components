from discord_components.component import _get_component_type, ActionRow
from discord_slash.model import SlashMessage

__all__ = ("ComponentSlashMessage",)


class ComponentSlashMessage(SlashMessage):
    def __init__(self, *, data: dict, **kwargs):
        super().__init__(data=data, **kwargs)

        components = []
        for i in data["components"]:
            if i["type"] == 1:
                components.append(ActionRow())
                for j in i["components"]:
                    components[-1].append(_get_component_type(j["type"]).from_json(j))
            else:
                components.append(_get_component_type(i["type"]).from_json(i))
        self.components = components
